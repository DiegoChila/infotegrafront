import React, { useEffect, useState } from 'react'
import Header from './Header';
import axios from 'axios';

export default function Stundent({history}) {
    const initValue = {
        name: '',
        lastName: '',
        direction: '',
        tel: '',
        cityResidence: '',
        cityOrigin: '',
        nationality: '',
        role: '',
        email: '',
        code: '',
        programsUser: [],
    };

    const initValueError = {
        name: false,
        lastName: false,
        direction: false,
        tel: false,
        cityResidence: false,
        cityOrigin: false,
        nationality: false,
    };

    const [formData, setFormData] = useState(initValue);
    const [formDataError, setFormDataError] = useState(initValueError);

    useEffect(() => {
        const {token, id} = JSON.parse(localStorage.getItem('state'));
        axios.get(`http://127.0.0.1:8000/api/user/${id}`, {
            headers: {
                'Authorization': `Bearer ${token}`,
            }
        })
        .then(resp => {
            const {user} = resp.data;
            const {programs} = resp.data;
            setFormData({
                ...formData,
                name: user.name,
                lastName: user.lastName,
                direction: user.direction,
                tel: user.tel,
                cityResidence: user.cityResidence,
                cityOrigin: user.cityOrigin,
                nationality: user.nationality,
                role: user.role,
                email: user.email,
                code: user.code,
                programsUser: programs
            })
        });        
    }, []);

    const logout = () => {
        localStorage.removeItem('state');
        history.replace('/');
    };

    const changeData = ({target}) => {
        setFormData({
            ...formData,
            [target.id]: target.value
        });
    };

    const update = (e) => {
        e.preventDefault();
        let errors = {...initValueError};
        let updateUser = true
        if (formData.name === '')
        {
            errors.name = true;
            updateUser = false;
        }
        if (formData.lastName === '')
        {
            errors.lastName = true;
            updateUser = false;
        }
        if (formData.nationality === '')
        {
            errors.nationality = true;
            updateUser = false;
        }
        if (formData.cityOrigin === '')
        {
            errors.cityOrigin = true;
            updateUser = false;
        }
        if (formData.cityResidence === '')
        {
            errors.cityResidence = true;
            updateUser = false;
        }
        if (formData.direction === '')
        {
            errors.direction = true;
            updateUser = false;
        }
        if (formData.tel === '')
        {
            errors.tel = true;
            updateUser = false;
        }

        setFormDataError(errors)

        if (updateUser)
        {
            const {token, id} = JSON.parse(localStorage.getItem('state'));

            const params = {
                name: formData.name,
                lastName: formData.lastName,
                direction: formData.direction,
                tel: formData.tel,
                cityResidence: formData.cityResidence,
                cityOrigin: formData.cityOrigin,
                nationality: formData.nationality,
            };

            axios.put(`http://127.0.0.1:8000/api/user/${id}`, params, {
                headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`}
            })
            .then(resp => {
                const {user} = resp.data;
                setFormData({
                    ...formData,
                    name: user.name,
                    lastName: user.lastName,
                    direction: user.direction,
                    tel: user.tel,
                    cityResidence: user.cityResidence,
                    cityOrigin: user.cityOrigin,
                    nationality: user.nationality,
                    role: user.role,
                    email: user.email,
                    code: user.code
                })
            });
        }
    };

    return (
        <div>
            <Header logout={logout}/>
            <div className="container mt-5">
                <form>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="name">Nombres</label>
                            <input type="text" className="form-control" id="name" value={formData.name} onChange={changeData} style={formDataError.name ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="lastName">Apellidos</label>
                            <input type="text" className="form-control" id="lastName" value={formData.lastName} onChange={changeData} style={formDataError.lastName ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="nationality">Nacionalidad</label>
                            <input type="text" className="form-control" id="nationality" value={formData.nationality} onChange={changeData} style={formDataError.nationality ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="cityOrigin">Ciudad de origen</label>
                            <input type="text" className="form-control" id="cityOrigin" value={formData.cityOrigin} onChange={changeData} style={formDataError.cityOrigin ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="cityResidence">Ciudad de residencia</label>
                            <input type="text" className="form-control" id="cityResidence" value={formData.cityResidence} onChange={changeData} style={formDataError.cityResidence ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="direction">Dirección</label>
                            <input type="text" className="form-control" id="direction" value={formData.direction} onChange={changeData} style={formDataError.direction ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="tel">Telefeono</label>
                            <input type="number" className="form-control" id="tel" value={formData.tel} onChange={changeData} style={formDataError.tel ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="role">Rol</label>
                            <div type="number" className="form-control" id="role" >{(formData.role === 1) ? 'Administrador' : 'Estudiante'}</div>
                        </div>
                    </div>
                    <div className="form-group">
                        <label for="email">Correo electronico</label>
                        <div type="email" className="form-control" id="email">{formData.email}</div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-3">
                            <label for="code">Codigo</label>
                            <div type="number" className="form-control" id="code" >{formData.code}</div>
                        </div>
                        <div className="form-group col-md-9">
                            <label for="programs">Programas</label>
                            <div type="number" className="form-control" id="programs" >{formData.programsUser.map(item => (` ${item},`))}</div>
                        </div>
                    </div>
                    <div className="col-md-2 mx-auto">
                        <button className="btn btn-success col-12 mx-auto" onClick={update}>Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    )
}
