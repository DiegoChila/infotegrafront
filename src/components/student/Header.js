import React from 'react'

export default function Header({logout}) {
    return (
        <div className="navbar navbar-light bg-light justify-content-between">
            <div className="container">
                <a className="navbar-brand">INFOTEGRA</a>
                <form className="form-inline">
                    <button className="btn btn-outline-danger my-2 my-sm-0" onClick={logout}>Logout</button>
                </form>
            </div>
        </div>
    )
}
