import React, { useState } from 'react';
import Header from './Header'
import Programs from './programs/Programs';
import Stundets from './stundets/Stundets';

export default function Admin({history, match}) {
    const [showPrograms, setShowPrograms] = useState(true);
    // console.log(match)

    const logout = () => {
        localStorage.removeItem('state');
        history.replace('/');
    }

    return (
        <div>
            <Header setShowPrograms={setShowPrograms} showPrograms={showPrograms} logout={logout}/>
            {
                (showPrograms) ? 
                <Programs history={history}/> :
                <Stundets />
            }
        </div>
    )
}
