import React, { useEffect, useState } from 'react';
import axios from 'axios';
import AddProgram from './AddProgram';

export default function Stundets() {
    const initStateAddProgram = {
        view: false,
        id: 0
    };

    const [students, setStudents] = useState([]);
    const [addProgramView, setAddProgramView] = useState(initStateAddProgram);

    useEffect(() => {
        const {token} = JSON.parse(localStorage.getItem('state'));
        axios.get(`http://127.0.0.1:8000/api/user`, {
            headers: {
                'Authorization': `Bearer ${token}`,
            }
        })
        .then(resp => {
            setStudents(resp.data.stundent);
        });        
    }, []);

    const addProgram = (e, id) => {
        e.preventDefault();
        console.log(id);
        setAddProgramView({
            ...addProgramView,
            view: true,
            id: id
        })
    };

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th scope="col">Codigo</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Adicionar curso</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        students.map((item) => {
                            return (
                                <tr key={item.id}>
                                    <td>{item.code}</td>
                                    <td>{item.name}</td>
                                    <td>{item.lastName}</td>
                                    <td><button className="btn btn-outline-success" onClick={(e) => addProgram(e, item.id)}>Adicionar</button></td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
            {
                addProgramView.view && <AddProgram setAddProgramView={setAddProgramView} addProgramView={addProgramView}/>
            }
        </div>
    )
}
