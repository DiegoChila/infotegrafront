import React, { useEffect, useState } from 'react';
import axios from 'axios';

export default function AddProgram({setAddProgramView, addProgramView}) {
    const initValue = {
        id_program: ''
    };

    const initValueError = {
        id_program: false,
    };

    const [programs, setPrograms] = useState([]);
    const [formData, setFormData] = useState(initValue);
    const [formDataError, setFormDataError] = useState(initValueError);

    useEffect(() => {
        axios.get(`http://127.0.0.1:8000/api/program`)
        .then(resp => {
            setPrograms(resp.data.programs);
        });
    }, []);

    const cancelAddProgram = (e) => {
        e.preventDefault();
        cancel();
    };

    const cancel = () => {
        setAddProgramView({
            ...addProgramView,
            view: false,
            id: ''
        });
    }

    const changeData = ({target}) => {
        setFormData({
            ...formData,
            [target.id]: target.value
        });
    };

    const addProgramByUser = (e) => {
        e.preventDefault();
        let errors = {...initValueError};
        let addProgram = true;
        if (formData.id_program === '') 
        {
            errors.id_program = true;
            addProgram = false;
        }
        if (addProgram)
        {
            const {token} = JSON.parse(localStorage.getItem('state'));
            const data = new FormData();
            data.append('id_program', formData.id_program); 
            data.append('id_user', addProgramView.id);
            axios.post(`http://127.0.0.1:8000/api/programbyuser`, data, {
                headers: {
                    'Content-Type': 'x-www-form-urlencoded',
                    'Authorization': `Bearer ${token}`,
                }
            })
            .then(resp => {
                if (resp.data.success) cancel();
                else 
                {
                    if (resp.data.error === 'ProgramByUser')
                    {
                        console.log('ya tiene este curso')
                        cancel();
                    }
                }
            });
        }
        else
        {
            setFormDataError(errors);
        }
    };

    return (
        <div className="background">
            <div className="register">
                <form>
                    <div className="form-group">
                        <label for="id_program">Carrera</label>
                        <select id="id_program" className="form-control" value={formData.id_program} onChange={changeData} style={formDataError.id_program ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}>
                            <option value="" selected>Seleccione...</option>
                            {
                                programs.map((item) => <option value={item.id} key={item.id}>{item.program}</option>)
                            }
                        </select>
                    </div>
                    <div className="form-group">
                        <div className="buttons">
                            <button className="btn btn-outline-primary ml-1" onClick={cancelAddProgram}>Cancelar</button>
                            <button className="btn btn-primary mr-1" onClick={addProgramByUser}>Registrarse</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
