import React from 'react'

export default function Header({setShowPrograms, showPrograms, logout}) {
    const changeView = (e) => {
        e.preventDefault();
        setShowPrograms(!showPrograms);
    };

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand">INFOTEGRA</a>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link" onClick={changeView} href="">Carreras </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" onClick={changeView} href="">Estudiantes </a>
                    </li>
                </ul>
            </div>
            <form className="form-inline">
                <button className="btn btn-outline-danger my-2 my-sm-0" onClick={logout}>Logout</button>
            </form>
        </nav>
    )
}
