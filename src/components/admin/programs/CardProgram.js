import React, { useState } from 'react';
import './style/cardProgram.css';
import axios from 'axios';

export default function CardProgram({item, setPrograms, programs, history}) {
    const {program, semesters, imgSrc, faculty, campus, id} = item;

    const deleteProgram = (e) => {
        e.preventDefault();
        console.log(id)
        const {token} = JSON.parse(localStorage.getItem('state'));
        axios.delete(`http://127.0.0.1:8000/api/program/${id}`, {
            headers: {
                'Authorization': `Bearer ${token}`,
            }
        })
        .then(resp => {
            if (resp.data.success)
            {
                let programsLocal = [];
                programs.map((item) => {
                    if (item.id.toString() !== resp.data.id) 
                    {
                        programsLocal.push(item);
                    }
                });
                console.log(programsLocal)
                setPrograms(programsLocal);
            }
        });
    };

    const selectProgram = () => {
        history.push(`/program/${id}`)
    };

    return (
        <div className="card_program_st card col-md-3 mt-3 ml-1 mr-1" onClick={selectProgram}>
            <img className="card-img-top" src={imgSrc ? imgSrc : 'https://image.freepik.com/vector-gratis/dibujo-universidad_23-2147501131.jpg'} alt="Card image cap"/>
            <div className="card-body">
                <dl>
                    <dt><b>{program}</b></dt>
                    <dd>{`${semesters} semestres`}</dd>
                    <dd>{faculty}</dd>
                    <dd>{campus}</dd>
                </dl>
            </div>
            <div className="cont_btn_st">
                <div className="button_st">
                    <button className="btn btn-outline-danger mb-3" onClick={deleteProgram}>Eliminar</button>
                </div>
            </div>
        </div>
    )
}
