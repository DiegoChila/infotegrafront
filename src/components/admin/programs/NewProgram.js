import React, { useState } from 'react';
import axios from 'axios';

export default function NewProgram({setNewProgram, setPrograms, programs}) {
    const initialState = {
        image: '',
        program: '',
        semesters: '',
        campus: '',
        faculty: '',
        selFile: ''
    };

    const [formData, setData] = useState(initialState);
    const [enableBtn, setEnableBtn] = useState(false);

    const cancelNewProgram = (e) => {
        e.preventDefault();
        setNewProgram(false);
    };

    const selectImage = () => {
        document.querySelector('#fileNewSelector').click();
    };

    const imgSel = (e) => {
        setData({...formData, image: e.target.files[0].name, selFile: (e.target.files)[0]});
    };

    const changeData = ({target}) => {
        setData({
            ...formData,
            [target.id]: target.value
        });
        if (formData.program !== '' && formData.semesters !== '' && formData.image !== '' && formData.campus !== '' && formData.faculty !== '') setEnableBtn(true);
        else setEnableBtn(false);
    };

    const createNewProgram = (e) => {
        e.preventDefault();
        const {token} = JSON.parse(localStorage.getItem('state'));

        const data = new FormData();
        data.append('image', formData.selFile); 
        data.append('program', formData.program); 
        data.append('semesters', formData.semesters); 
        data.append('campus', formData.campus); 
        data.append('faculty', formData.faculty); 

        axios.post(`http://127.0.0.1:8000/api/program`,data ,{
            headers: {
                'Content-Type': 'form-data',
                'Authorization': `Bearer ${token}`,
            }
        })
        .then(resp => {
            console.log(resp)
            if (resp.data.success)
            {
                setPrograms([...programs, resp.data.program]);
                setNewProgram(false)
            }
        });
    }

    return (
        <div className="background">
            <input id="fileNewSelector" accept="image/*" type="file" style={{display: 'none'}} onChange={imgSel}/>
            <div className="register">
                <form>
                    <div className="form-group">
                        <label for="program">Carrera</label>
                        <input type="text" className="form-control" id="program" placeholder="Ingenieria electronica" value={formData.program} onChange={changeData}/>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="campus">Sede</label>
                            <input type="text" className="form-control" id="campus" placeholder="Norte" value={formData.campus} onChange={changeData}/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="faculty">Facultad</label>
                            <input type="text" className="form-control" id="faculty" placeholder="Ingenieria" value={formData.faculty} onChange={changeData}/>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-8">
                            <label for="image">Imagen</label>
                            <div type="text" className="form-control" id="image" onClick={selectImage}>{formData.image ? formData.image : 'Seleccione una imagen'}</div>
                        </div>
                        <div className="form-group col-md-4">
                            <label for="semesters">Semestres</label>
                            <input type="number" className="form-control" id="semesters" placeholder="10" value={formData.semesters} onChange={changeData}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="buttons">
                            <button className="btn btn-outline-primary ml-1" onClick={cancelNewProgram}>Cancelar</button>
                            <button className="btn btn-primary mr-1" disabled={!enableBtn} onClick={createNewProgram}>Crear</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
