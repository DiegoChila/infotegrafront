import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Chart } from "react-google-charts";
import './style/graph.css'

export default function Graph() {
    const [programs, setPrograms] = useState([]);
    const [programsToShow, setProgramsToShow] = useState([]);
    const [viewGraph, setViewGraph] = useState(false);

    useEffect(() => {
        const {token} = JSON.parse(localStorage.getItem('state'));
        axios.get(`http://127.0.0.1:8000/api/programbyuser`, {
            headers: {
                'Authorization': `Bearer ${token}`,
            }
        })
        .then(resp => {
            setPrograms(resp.data.programs);
        });        
    }, []);

    useEffect(() => {
        let programArray = [];
        programArray.push(['Carrera', 'Estudiantes']);
        programs.map((item) => {
            programArray.push([item.program, item.count]);
        });
        setProgramsToShow(programArray);
        setViewGraph(true);
    }, [programs])

    return (
        <div className="graph">
            <div className="image">
                {
                    (viewGraph) && <Chart
                    width={'500px'}
                    height={'300px'}
                    chartType="PieChart"
                    loader={<div>Loading Chart</div>}
                    data={programsToShow}
                    options={{
                        title: 'Estudiantes por carrera',
                    }}
                    rootProps={{ 'data-testid': '1' }}
                />
                }
            </div>
        </div>
        
    )
}
