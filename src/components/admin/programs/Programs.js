import React, { useEffect, useState } from 'react';
import axios from 'axios';
import CardProgram from './CardProgram';
import './style/program.css'
import Graph from './Graph';
import NewProgram from './NewProgram';

export default function Programs({history}) {
    const [programs, setPrograms] = useState([]);
    const [newProgram, setNewProgram] = useState(false);
    // console.log(match)
    
    
    useEffect(() => {
        request();
    }, []);

    const request = () => {
        axios.get(`http://127.0.0.1:8000/api/program`)
        .then(resp => {
            setPrograms(resp.data.programs);
        });
    };

    const showNewProgram = (e) => {
        e.preventDefault();
        setNewProgram(true);
    }

    return (
        <div className="program">
            <div className="btn_st">   
                <button className="btn btn-success btn_st" onClick={showNewProgram}>Agregar Carrera</button>
            </div>
            
            {
                <div className="cardList row">
                    {programs.map((item) => {
                        return <CardProgram key={item.id} item={item} programs={programs} setPrograms={setPrograms} history={history}/>
                    })}
                </div>
            }
            <Graph />
            {
                newProgram && <NewProgram setNewProgram={setNewProgram} programs={programs} setPrograms={setPrograms}/>
            }
        </div>
    )
}
