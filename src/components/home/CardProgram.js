import React from 'react';
import './style/card.css';

export default function CardProgram({item}) {
    const {program, semesters, imgSrc, faculty, campus} = item;
    return (
        <div className="card_st card col-md-3 mt-3 ml-1 mr-1">
            <img className="card-img-top" src={imgSrc ? imgSrc : 'https://image.freepik.com/vector-gratis/dibujo-universidad_23-2147501131.jpg'} alt="Card image cap"/>
            <div className="card-body">
                <dl>
                    <dt><b>{program}</b></dt>
                    <dd>{`${semesters} semestres`}</dd>
                    <dd>{faculty}</dd>
                    <dd>{campus}</dd>
                </dl>
            </div>
        </div>
    )
}
