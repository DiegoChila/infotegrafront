import React, { useState } from 'react';
import './style/register.css';
import axios from 'axios';

export default function Register({setShowRegister, loginApp}) {
    const cancelRegister = (e) => {
        e.preventDefault();
        setShowRegister(false)
    };

    const initValue = {
        name: '',
        lastName: '',
        direction: '',
        tel: '',
        cityResidence: '',
        cityOrigin: '',
        nationality: '',
        role: '',
        email: '',
        password: '',
        passwordConfirm: '',
    };

    const initValueError = {
        name: false,
        lastName: false,
        direction: false,
        tel: false,
        cityResidence: false,
        cityOrigin: false,
        nationality: false,
        role: false,
        email: false,
        password: false,
        passwordConfirm: false,
    }

    const [formData, setFormData] = useState(initValue);
    const [formDataError, setFormDataError] = useState(initValueError);
    const [enableBtn, setEnableBtn] = useState(false);

    const validateEmail = (email) => {
        let regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email) ? true : false;
    };

    const changeData = ({target}) => {
        setFormData({
            ...formData,
            [target.id]: target.value
        });
        if (formData.name !== '' && formData.lastName !== '' && formData.direction !== '' && formData.tel !== '' && formData.cityResidence !== ''
        && formData.cityOrigin !== '' && formData.nationality !== '' && formData.role !== '' && formData.email !== '' && formData.password !== '' && formData.passwordConfirm !== '') setEnableBtn(true);
        else setEnableBtn(false);
    };

    const register = (e) => {
        e.preventDefault();
        let registerNewUser = true;
        let errors = {};
        let data = {...formData};
        if (isNaN(formData.tel)) 
        {
            registerNewUser = false;
            errors = {...errors, tel: true};
            data.tel = '';
        }
        if (formData.password !== formData.passwordConfirm)
        {
            registerNewUser = false;
            errors = {...errors, password: true};
            errors = {...errors, passwordConfirm: true};
            data.password = '';
            data.passwordConfirm = '';
        }
        if (!validateEmail(formData.email))
        {
            errors = {...errors, email: true};
            registerNewUser = false;
            data.email = '';
        }
        if (registerNewUser)
        {
            setFormDataError(errors);
            const params = {
                name: formData.name,
                lastName: formData.lastName,
                direction: formData.direction,
                tel: formData.tel,
                cityResidence: formData.cityResidence,
                cityOrigin: formData.cityOrigin,
                nationality: formData.nationality,
                role: formData.role,
                email: formData.email,
                password: formData.password,
            };
            const headers=  {
                'Content-Type': 'x-www-form-urlencoded',
            };
            axios.post(`http://127.0.0.1:8000/api/register`, params, headers)
            .then(resp => {
                console.log(resp)
                console.log(resp.data.success)
                if (resp.data.success)
                {
                    loginApp(resp.data.user.role, resp.data.user.id, resp.data.token)
                }
            });
        }
        else
        {
            setFormData(data);
            setFormDataError(errors);
            setEnableBtn(false);
        }
    };

    return (
        <div className="background">
            <div className="register">
                <form>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="name">Nombres</label>
                            <input type="text" className="form-control" id="name" placeholder="Pepito" onChange={changeData} value={formData.name} style={formDataError.name ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="lastName">Apellidos</label>
                            <input type="text" className="form-control" id="lastName" placeholder="Perez" onChange={changeData} value={formData.lastName} style={formDataError.lastName ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="nationality">Nacionalidad</label>
                            <input type="text" className="form-control" id="nationality" placeholder="Colombia" onChange={changeData} value={formData.nationality} style={formDataError.nationality ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="cityOrigin">Ciudad de origen</label>
                            <input type="text" className="form-control" id="cityOrigin" placeholder="Bogotá" onChange={changeData} value={formData.cityOrigin} style={formDataError.cityOrigin ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="cityResidence">Ciudad de residencia</label>
                            <input type="text" className="form-control" id="cityResidence" placeholder="Bogotá" onChange={changeData} value={formData.cityResidence} style={formDataError.cityResidence ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="direction">Dirección</label>
                            <input type="text" className="form-control" id="direction" placeholder="Dirección" onChange={changeData} value={formData.direction} style={formDataError.direction ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="tel">Telefeono</label>
                            <input type="number" className="form-control" id="tel" placeholder="3166486758" onChange={changeData} value={formData.tel} style={formDataError.tel ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="role">Rol</label>
                            <select id="role" className="form-control" onChange={changeData} value={formData.role} style={formDataError.role ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}>
                                <option value="" selected>Seleccione...</option>
                                <option value="1">Administrador</option>
                                <option value="2">Estudiante</option>
                            </select>
                        </div>
                    </div>
                    <div className="form-group">
                        <label for="email">Correo electronico</label>
                        <input type="email" className="form-control" id="email" placeholder="ejemplo@ejemplo.com" onChange={changeData} value={formData.email} style={formDataError.email ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label for="password">Contraseña</label>
                            <input type="password" className="form-control" id="password" placeholder="******" onChange={changeData} value={formData.password} style={formDataError.password ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="passwordConfirm">Repita la contraseña</label>
                            <input type="password" className="form-control" id="passwordConfirm" placeholder="******" onChange={changeData} value={formData.passwordConfirm} style={formDataError.passwordConfirm ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="buttons">
                            <button className="btn btn-outline-primary ml-1" onClick={cancelRegister}>Cancelar</button>
                            <button className="btn btn-primary mr-1" onClick={register} disabled={!enableBtn}>Registrarse</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
    )
}
