import React, { useEffect, useState } from 'react'
import Header from './Header'
import Login from './Login'
import Register from './Register';
import axios from 'axios';
import CardProgram from './CardProgram';
import './style/home.css';

export default function Home({history}) {
    const [showLogin, setShowLogin] = useState(false);
    const [showRegister, setShowRegister] = useState(false);
    const [programs, setPrograms] = useState([]);

    const loginApp = (role, id, token) => {
        const state = {
            id,
            token
        };
        localStorage.setItem('state', JSON.stringify(state));
        if (role === 1) history.replace('/admin');
        else history.replace('/student');
    }
    
    useEffect(() => {
        axios.get(`http://127.0.0.1:8000/api/program`)
        .then(resp => {
            setPrograms(resp.data.programs);
        });        
    }, []);

    return (
        <div className="home">
            <Header setShowLogin={setShowLogin} showLogin={showLogin} setShowRegister={setShowRegister} showRegister={showRegister}/>
            <div className="container">
                {
                    <div className="cardList row">
                        {programs.map((item) => {
                            return <CardProgram key={item.id} item={item}/>
                        })}
                    </div>
                }
            </div>
                {
                    showLogin && <Login setShowLogin={setShowLogin} loginApp={loginApp}/>
                }
                {
                    showRegister && <Register setShowRegister={setShowRegister} loginApp={loginApp}/>
                }
            
            
        </div>
    )
}
