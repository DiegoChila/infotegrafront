import React from 'react'

export default function Header({setShowLogin, showLogin, showRegister, setShowRegister}) {
    const showLoginButton = (e) => {
        e.preventDefault();
        setShowRegister(false);
        setShowLogin(!showLogin);
    };

    const showRegisterButton = (e) => {
        e.preventDefault();
        setShowLogin(false);
        setShowRegister(!showRegister)
    };

    return (
        <div className="navbar navbar-light bg-light justify-content-between">
            <div className="container">
                <a className="navbar-brand">INFOTEGRA</a>
                <form className="form-inline">
                    <button className="btn btn-success mr-sm-2" onClick={showRegisterButton}>Registarse</button>
                    <button className="btn btn-outline-success my-2 my-sm-0" onClick={showLoginButton}>Login</button>
                </form>
            </div>
        </div>
    )
}
