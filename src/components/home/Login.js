import React, { useState } from 'react';
import ReCAPTCHA from "react-google-recaptcha";
import './style/login.css';
import axios from 'axios';

export default function Login({setShowLogin, loginApp}) {
    const recaptchaRef = React.createRef();

    const initialState = {
        email: '',
        password: ''
    };

    const initialStateError = {
        email: false,
        password: false
    };

    const [formData, setFormData] = useState(initialState);
    const [formDataError, setFormDataError] = useState(initialStateError);
    const [enableButton, setEnableButton] = useState(false);

    const validateEmail = (email) => {
        let regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email) ? true : false;
    };

    const cancelLogin = (e) => {
        e.preventDefault();
        setShowLogin(false);
    };

    const changeData = ({target}) => {
        setFormData({
            ...formData,
            [target.id]: target.value
        });
        if (formData.email === '' && formData.password === '') setEnableButton(false);
    };

    const login = (e) => {
        e.preventDefault();
        let loginBoolean = true;
        let errors = {};
        let data = {...formData};
        console.log(errors)
        console.log(data)
        if (!validateEmail(formData.email))
        {
            errors = {...errors, email: true};
            loginBoolean = false;
            data.email = '';
        }
        if (loginBoolean)
        {
            setFormDataError(errors);
            loginRequest();
        }
        else
        {
            setFormData(data);
            setFormDataError(errors);
            setEnableButton(false);
        }
    };

    const captchaSelected = (value) => {
        const params = {
            response: value
        };
        const headers=  {
            'Content-Type': 'x-www-form-urlencoded',
        };
        axios.post(`http://127.0.0.1:8000/api/validateCaptcha`, params, headers)
        .then(resp => {
            if (resp.data.success) setEnableButton(true);
        }); 
    };

    const loginRequest = () => {
        const params = {
            email: formData.email,
            password: formData.password
        };
        const headers=  {
            'Content-Type': 'x-www-form-urlencoded',
        };
        axios.post(`http://127.0.0.1:8000/api/login`, params, headers)
        .then(resp => {
            if (resp.data.login)
            {
                loginApp(resp.data.user.role, resp.data.user.id, resp.data.token)
            }
        }); 
    };

    return (
        <div className="background">
            <div className="login">
                <form>
                    <div className="form-group">
                        <label for="email">Correo electronico</label>
                        <input type="email" className="form-control" id="email" placeholder="ejemplo@ejemplo.com" value={formData.email} onChange={changeData} style={formDataError.email ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                    </div>
                    <div className="form-group">
                        <label for="password">Contraseña</label>
                        <input type="password" className="form-control" id="password" placeholder="******" value={formData.password} onChange={changeData} style={formDataError.password ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                    </div>
                    <div className="mt-3 mb-3">
                        <ReCAPTCHA
                            ref={recaptchaRef}
                            sitekey="6LcxY1QaAAAAALIiXWewQsyYGKWvx9I_lne6LCym"
                            onChange={captchaSelected}
                        />
                    </div>
                    <div className="form-group">
                        <div className="buttons">
                            <button className="btn btn-outline-primary ml-1" onClick={cancelLogin}>Cancelar</button>
                            <button className="btn btn-primary mr-1" disabled={!enableButton} onClick={login}>Login</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
    )
}
