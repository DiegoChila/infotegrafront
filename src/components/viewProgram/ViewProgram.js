import React, { useEffect, useState } from 'react'
import axios from 'axios';
import './style/viewProgram.css'
import Header from './Header';

export default function ViewProgram({match, history}) {
    const initialState = {
        campus: '',
        faculty: '',
        program: '',
        semesters: '',
        imgSrc: '',
    };

    const initialStateError = {
        campus: false,
        faculty: false,
        program: false,
        semesters: false,
    };

    const id = match.params.id_program;
    const [formData, setFormData] = useState(initialState);
    const [formDataError, setFormDataError] = useState(initialStateError);

    useEffect(() => {
        const {token} = JSON.parse(localStorage.getItem('state'));
        axios.get(`http://127.0.0.1:8000/api/program/${id}`, {
            headers: {
                'Authorization': `Bearer ${token}`,
            }
        })
        .then(resp => {
            console.log(resp)
            if (resp.data)
            {
                setFormData({
                    campus: resp.data.program.campus,
                    faculty: resp.data.program.faculty,
                    program: resp.data.program.program,
                    semesters: resp.data.program.semesters,
                    imgSrc: resp.data.program.imgSrc,
                });
            }
        });        
    }, []);

    const changeData = ({target}) => {
        setFormData({
            ...formData,
            [target.id]: target.value
        });
    };

    const selectImage = () => {
        document.querySelector('#fileNewSelector').click();
    };

    const imgSel = (e) => {
        console.log('id: ',id);

        const {token} = JSON.parse(localStorage.getItem('state'));
        const file = (e.target.files)[0];

        const data = new FormData();
        data.append('id', id); 
        data.append('image', file); 

        axios.post(`http://127.0.0.1:8000/api/program/uploadImage`,data ,{
            headers: {
                'Content-Type': 'form-data',
                'Authorization': `Bearer ${token}`,
            }
        })
        .then(resp => {
            console.log(resp)
            if (resp.data.success)
            {
                setFormData({
                    ...formData,
                    imgSrc: resp.data.imgSrc
                })
            }
        });
    };

    const update = (e) => {
        e.preventDefault();
        let errors = {...initialStateError};
        let updateUser = true

        setFormDataError(errors);

        if (updateUser)
        {
            const {token} = JSON.parse(localStorage.getItem('state'));

            const params = {
                program: formData.program,
                semesters: formData.semesters,
                campus: formData.campus,
                faculty: formData.faculty
            };

            axios.put(`http://127.0.0.1:8000/api/program/${id}`, params, {
                headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`}
            })
            .then(resp => {
                console.log(resp)
                if (resp.data.success)
                {
                    setFormData({
                        campus: resp.data.program.campus,
                        faculty: resp.data.program.faculty,
                        program: resp.data.program.program,
                        semesters: resp.data.program.semesters,
                        imgSrc: resp.data.program.imgSrc,
                    });
                }
            });
        }
    };

    const backToPrograms = () => {
        history.push('/admin');
    };

    const logout = () => {
        localStorage.removeItem('state');
        history.replace('/');
    };

    return (
        <div>
            <Header logout={logout}/>
            <input id="fileNewSelector" accept="image/*" type="file" style={{display: 'none'}} onChange={imgSel}/>
            <div className="container mt-5">
                <div className="ml-4" onClick={backToPrograms}><a href=""><h2>Ver programas</h2></a></div>
                <div className="row mt-4">
                    <div className="col-md-4">
                        <div className="cont_img_vp_st" onClick={selectImage}>
                            <img src={formData.imgSrc === '' ? 'https://image.freepik.com/vector-gratis/dibujo-universidad_23-2147501131.jpg' : formData.imgSrc} className="img_vp_st"/>
                        </div>
                    </div>
                    <div className="col-md-8">
                        <form>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label for="campus">Sede</label>
                                    <input type="text" className="form-control" id="campus" placeholder="Norte" value={formData.campus} onChange={changeData} style={formDataError.campus ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label for="faculty">Facultad</label>
                                    <input type="text" className="form-control" id="faculty" placeholder="Ingenieria" value={formData.faculty} onChange={changeData} style={formDataError.faculty ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-8">
                                    <label for="program">Carrera</label>
                                    <input type="text" className="form-control" id="program" placeholder="Ingenieria electronica" value={formData.program} onChange={changeData} style={formDataError.program ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                                </div>
                                <div className="form-group col-md-4">
                                    <label for="semesters">Semestres</label>
                                    <input type="number" className="form-control" id="semesters" placeholder="10" value={formData.semesters} onChange={changeData} style={formDataError.semesters ? {borderBlockColor: 'red', borderBlockWidth: '2px'} : {}}/>
                                </div>
                            </div>
                            <div className="col-md-4 mx-auto">
                                <button className="btn btn-success" onClick={update}>Actualizar</button>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
           
        </div>
    )
}
