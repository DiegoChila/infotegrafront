import React from 'react'

export default function Header({logout}) {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container">
                <a className="navbar-brand">INFOTEGRA</a>
                <form className="form-inline">
                    <button className="btn btn-outline-danger my-2 my-sm-0" onClick={logout}>Logout</button>
                </form>
            </div>
        </nav>
    )
}
