import React from 'react'
import {BrowserRouter as Router, Switch, Redirect, Route} from 'react-router-dom';
import Admin from '../components/admin/Admin';
import Home from '../components/home/Home'
import Stundent from '../components/student/Stundent';
import ViewProgram from '../components/viewProgram/ViewProgram';

export default function AppRoute() {
    return (
        <Router>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route path='/admin' component={Admin}/>
                <Route path='/student' component={Stundent}/>
                <Route path='/program/:id_program' component={ViewProgram}/>
            </Switch>
        </Router>
    )
}
