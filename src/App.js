import React from 'react';
import Home from './components/home/Home';
import AppRoute from './routes/AppRoute';

export default function App() {
    return (
        <AppRoute />
    )
}
